#pragma once
template<typename T>
class A {
public:
A() = default;
~A() = default;
auto doSomething(T a, T b) -> T {
 return a*b;
}
};