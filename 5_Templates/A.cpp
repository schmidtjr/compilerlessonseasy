#include "A.h"

auto doSomethingSpecial(int a, int b) -> int {
	A<int> a_int;
	return a_int.doSomething(a,b);
}

auto doSomethingSpecial(float a, float b) -> float {
	A<float> a_float;
	return a_float.doSomething(a,b);
}