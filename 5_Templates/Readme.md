# Templates in object files
this folder guides you through the cost of having templates.
If you know the template instances your program needs, so can reduce compile cost by explicit instantiation (more on that in the next folders).


# demonstration
## create and investigate object files
* show A.cpp and A.h
	* dog A.h; dog A.cpp;
	* A.cpp has useless free methods that no one can reach. let's assume A is a library that uses some A<xx>s
* create object file
	* g++ -c -o A.o A.cpp
* show object file A
	* nm -C -A A.o
	* A.o contains code for doSomethingSpecial(int) and (float)
	* A.o contains *WEAK* symbols for A<float> and A<int>
	* means during linking they might be used from somewhere else, can be overriden
* show B
	* dog B.h; dog B.cpp;
	* highlight new forward declare for templated class
* create object files for both
	* g++ -c -o A.o A.cpp; g++ -c -o B.o B.cpp
* show object files for both
	* nm -C -A A.o B.o
	* watch out for 2 weak symbols for A<int>
* finally, show main and compile all together and analyze symbols
	* dog main.h; dog main.cpp
	* g++ -c -fno-exceptions -o A.o A.cpp; g++ -c -fno-exceptions -o B.o B.cpp; g++ -c -fno-exceptions -o main.o main.cpp;
	* (using noexcept for better comparison if compiler would decide to include exception features)
	* nm -C -A A.o B.o main.o 
	* --> 3 times weak symbol for A<int> == 3 times compiled and thrown away during link phase
* for the sake of completeness, compile and investigate binary
	* g++ -o main main.cpp A.cpp B.cpp
	* nm -C -A main
	* only one weak symbol left
* inspect total size of object files
	* find . -type f -name "*.o" -exec du -c -b {} + | grep total$
	* 6832 Byte

optimization is shown in the next chapter (to avoid pollution of source files here)