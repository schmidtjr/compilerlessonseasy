# Forward Declarations
this folder is just for showing the usefulness of forwad declarations

# demonstration
## use include
* show A.cpp and A.h, B.h and B.cpp, main.cpp
	* dog A.h; dog A.cpp; dog B.h; dog B.cpp; dog main.cpp
* comile and run
	* g++ -o main main.cpp A.cpp B.cpp
* create intermediates
	* g++ -E A.cpp -o A.cpp.ii ; g++ -E B.cpp -o B.cpp.ii; g++ -E -o main.cpp.ii main.cpp 
* show intermediates
	* dog A.cpp.ii; dog B.cpp.ii; dog main.cpp.ii
	* highlight that there's class A in B.cpp.ii AND main.cpp.ii
* count lines for comparison
	* wc -l A.cpp.ii B.cpp.ii main.cpp.ii
	* 22. 29, 30
	* 80 total

## use forward declaration
* modify B.h to use forward declaration
	* comment "include A.h"
	* uncomment "class A;"
* comile and run
	* g++ -o main main.cpp A.cpp B.cpp
* create intermediates
	* g++ -E A.cpp -o A.cpp.ii ; g++ -E B.cpp -o B.cpp.ii; g++ -E -o main.cpp.ii main.cpp 
* show intermediates
	* dog A.cpp.ii; dog B.cpp.ii; dog main.cpp.ii
	* highlight that there's class A only in B.cpp.ii AND NOT main.cpp.ii
* count lines
	* wc -l A.cpp.ii B.cpp.ii main.cpp.ii
	* 22, 29, 30
	* 74 total
