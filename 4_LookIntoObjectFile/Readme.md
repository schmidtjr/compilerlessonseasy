# First look at object files
this folder is an example folder to have a look into some simple object files

# demonstration
## create and investigate object files
* show A.cpp and A.h, B.h and B.cpp, main.cpp
	* dog A.h; dog A.cpp; dog B.h; dog B.cpp; dog main.cpp
* comile and run
	* g++ -o main main.cpp A.cpp B.cpp
	* oh it shows an error. Now main wants to activelly use A, so it must know the definition!
	* add definition (uncomment include A on main.cpp)
* recompile, run
* create object files
	* g++ -c -o A.o A.cpp; g++ -c -o B.o B.cpp; g++ -c -o main.o main.cpp
* show object file A
	* nm A.o
	* analyze: 
		* mangled names
* show object files for all generated files, mangled and with filename
	* nm -C -A A.o B.o main.o
	* A contains code for mysum, A::doSomething
	* B contains code for B() and B~()
	* main contains code for localfunction and main
	* main contains Undefined symbols for B and A::doSomething
		* those must be provided by other compile units
		* compiling only main.cpp yields an error (g++ main.cpp)
