#pragma once
class A {
public:
A() = default;
~A() = default;
auto doSomething(int a, int b) -> int;
};