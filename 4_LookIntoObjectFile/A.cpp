#include "A.h"


#define X 66

auto mysum(int a, int b) -> int {

	return X*(a+b);
}

auto A::doSomething(int a, int b) -> int {
	return mysum(a, b);
}