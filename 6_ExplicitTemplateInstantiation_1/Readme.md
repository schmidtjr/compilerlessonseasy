# Forward Declarations
This folder is based on folder "5_Templates". 
It adds upon this under the assumption that we know we have A<int> instances.
Let's try to avoid unnecessary compilation entities with c++11s extern template functionality.

# demonstration
## extern template, definition stays in header
* show A.cpp, A.h, B.cpp, B.h, and main.cpp
	* dog A.h; dog A.cpp; dog B.h; dog B.cpp; dog main.cpp
	* see the added "template class..." in A.cpp
	* see the added "extern template class..." in all users of A: B.cpp and main.cpp
* create object files
	* g++ -c -fno-exceptions -o A.o A.cpp; g++ -c -fno-exceptions -o B.o B.cpp; g++ -c -fno-exceptions -o main.o main.cpp;
	* (using noexcept for better comparison if compiler would decide to include exception features)
* inspect object files
	* nm -C -A A.o B.o main.o 
	* only 1 weak symbol for A<int>
	* the other A<int> symbols are undefined and hence not compiled in this compilation unit
* inspect total size of object files
	* find . -type f -name "*.o" -exec du -c -b {} + | grep total$
	* 6288 Byte
### Discussion of this approach
* every includer *must* include "extern template class...". If he forgets it, there's no benefit at all