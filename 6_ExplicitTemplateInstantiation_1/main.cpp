#include "B.h"
#include "A.h"

// since we don't want to instantiate A<int> again, we need to tell the compiler it is instantiated somewhere else
extern template class A<int>;

auto localFunction(int a) -> void {
	int b = a*2;
}

int main(int argc, char* argv[]) {
	B b;
	b.m_a->doSomething(1,2);
	A<char> a_char;
	a_char.doSomething('a','b');
	return 0;
}
