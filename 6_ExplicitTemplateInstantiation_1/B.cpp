#include "B.h"
#include "A.h"

// since we don't want to instantiate A<int> again, we need to tell the compiler it is instantiated somewhere else
extern template class A<int>;

B::B() {m_a->doSomething(1,2);}
B::~B() {}