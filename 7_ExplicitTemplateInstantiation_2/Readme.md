# Forward Declarations
This folder is based on folder "5_Templates".
It adds upon this under the assumption that we know we have A<int> instances.
Let's try to avoid unnecessary compilation entities with another approach.

The difference to folder "6_ExplicitTemplateInstantiation_1" is, that we moved the definition now out of the (template) header.


# demonstration
## create and investigate object files
* show A.cpp, A.h, B.cpp, B.h, and main.cpp
	* dog A.h; dog A.cpp; dog B.h; dog B.cpp; dog main.cpp
	* see that definition of doSomething is moved from A.h to A.cpp
	* see the added "template class..." in A.cpp
	* see that we haven't added extern template class in B.cpp or main.cpp!
* compile and run
	* g++ -o main main.cpp A.cpp B.cpp
	* oh oh, no explicit insantiation for A<char>
* fix it by uncommenting "template class A<char> in A.cpp
* create object files
	* g++ -c -fno-exceptions -o A.o A.cpp; g++ -c -fno-exceptions -o B.o B.cpp; g++ -c -fno-exceptions -o main.o main.cpp;
	* (using noexcept for better comparison if compiler would decide to include exception features)
* inspect object files
	* nm -C -A A.o B.o main.o 
	* only 1 weak symbol for A<int>
	* the other A<int> symbols are undefined and hence not compiled in this compilation unit
 total size of object files
	* find . -type f -name "*.o" -exec du -c -b {} + | grep total$
	* 6336 Byte

### Discussion of this approach
* you *must* explicitly instantiate all required types. No additional types possible
	* this means, that all types must be known to the template library
	* (can be good or bad)