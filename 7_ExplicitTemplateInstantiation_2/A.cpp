#include "A.h"

template<typename T>
auto A<T>::doSomething(T a, T b) -> T {
 return a*b;
}

// we know we will have some A<int> let's explicitly instantiate them once
template class A<int>;

// well. with this approach we MUST explicitly instantiate all possible instantiations. 
// template class A<char>;

auto doSomethingSpecial(int a, int b) -> int {
	A<int> a_int;
	return a_int.doSomething(a,b);
}

auto doSomethingSpecial(float a, float b) -> float {
	A<float> a_float;
	return a_float.doSomething(a,b);
}