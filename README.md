# CompilerLessonsEasy

(c++ content)

some simple examples for compiler optimizations or good practices

this repo shows minimzed examples to teach:
- what the preprocessor does
- what forward declarations are and what's the benefit
- what explicit template instantiation is an what's the benefit

# special commands
The Readme.md files often contain a command called "dog". It's a simple alias to print something to console with syntax highlighting.

You can either ignore it and use whatever you want to display files (IDE, cat,...) or try it out :).

* apt get install highlight
* create an alias:
	* alias dog='highlight -O xterm256 -l --stdout --force'