# PreProcessor
this folder is just for showing the plain basics of the PreProcessor, as there are:
- textual replacement and combination with includes
- loading of include files
- textual replacement of includes (recursive)
- header guards
- massive impact of what you include

# "presentation mode"
## simple textual replacement
* show A.cpp and A.h
	* dog A.h; dog A.cpp
* let preprocessor create intermediate file
	* g++ -E A.cpp -o A.cpp.ii
* inspect A.cpp.ii
	* dog A.cpp.ii
	* content of header is copied to resulting ii file
	* the define "X" was textually replaced

## loading of include files
* uncomment "include "C.h" from A.h
* g++ -E A.cpp -o A.cpp.ii
* discuss error of C not found
* comment out include C (or revert)

## textual replacement of includes (recursive)
* show math.h, show main.cpp
	* dog math.h; dog main.cpp
	* notice that main includes A and math
* create intermediate
	* g++ -E main.cpp -o main.cpp.ii
* show main.cpp.ii
	* dog main.cpp.ii
	* notice that the intermediate contains content of A.h, math.h and main.cpp
* now uncomment "include A.h" from math.h
* rebuild intermediate
	* g++ -E main.cpp -o main.cpp.ii
* show main.cpp.ii
	* dog main.cpp.ii
	* notice the *duplicate* include of A.h content
* try compile
	* g++ -o main A.cpp main.cpp 

(bonus)
* modify main to exclude math.h but include A again
-> rebuild intermedaite, show, won't compile

## header guards
* introduce header guard to A
	* uncomment pragma once
* recompile, et voila
	* g++ -o main A.cpp main.cpp 
	* (if wanted show intermediate)
		* g++ -E main.cpp -o main.cpp.ii

## massive impact
* show line count of main.cpp.ii
	* 43
* introduce std::cout output
* compile, run, voila
* regenerate intermediate, show wc -l
	* 28179