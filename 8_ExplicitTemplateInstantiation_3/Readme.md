# Forward Declarations
tThis folder is based on folder "5_Templates".
It adds upon this under the assumption that we know we have A<int> instances.
Let's try to avoid unnecessary compilation entities with another approach.

In this scenario, the template class definition stays at the header file. But we add an "extern template class..." to the template header itself, thus no need to have it at the caller side.


# demonstration
## create and investigate object files
* show A.cpp, A.h, B.cpp, B.h, and main.cpp
	* dog A.h; dog A.cpp; dog B.h; dog B.cpp; dog main.cpp
	* see that definition of doSomething is at A.h
	* see the added "extern template class..." in A.h
	* see that A.cpp contains explicit instantiation
	* see that we haven't added extern template class in B.cpp or main.cpp!
* create object files
	* g++ -c -fno-exceptions -o A.o A.cpp; g++ -c -fno-exceptions -o B.o B.cpp; g++ -c -fno-exceptions -o main.o main.cpp;
	* (using noexcept for better comparison if compiler would decide to include exception features)
* inspect object files
	* nm -C -A A.o B.o main.o 
	* only 1 weak symbol for A<int>
	* the other A<int> symbols are undefined and hence not compiled in this compilation unit
 total size of object files
	* find . -type f -name "*.o" -exec du -c -b {} + | grep total$
	* 6288 Byte

### Discussion of this approach
* definition stays in header, changes to header might lead to huge recompiles
* if header only lib, external projects must perform the explicit instantiation on their own
* if using specific types from your project, you *must* include them in the template header. Forward declare won't work!