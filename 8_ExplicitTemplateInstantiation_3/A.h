#pragma once
template<typename T>
class A {
public:
A() = default;
~A() = default;
auto doSomething(T a, T b) -> T {
 return a*b;
}
};

// publicly available extern definition, no need to add it on each caller
extern template class A<int>;