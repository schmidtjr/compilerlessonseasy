# Extreme Forward Declarations
previous learning for forward declarations was "soft" in terms of numbers, but hopefully showed the aspects of forward declarations.
In this episode, we are going to have a quick look at a more practical example. 

ostream is one of the standard library classes that can't be forward declared easily (templated multiple times, like std::string).
therefore a special forward declare header exists: iosfwd, let's see the difference

# demonstration
## use include
* show B.h and B.cpp, main.cpp
	* dog B.h; dog B.cpp; dog main.cpp
* compile and run
	* g++ -o main main.cpp B.cpp
* create intermediates
	* g++ -E B.cpp -o B.cpp.ii; g++ -E -o main.cpp.ii main.cpp 
* count lines
	* wc -l B.cpp.ii main.cpp.ii
	* 53.225 total

## use forward declaration
* modify B.h to use forward declaration
	* comment "include ostream"
	* uncomment "include iosfwd"
* compile and run
	* g++ -o main main.cpp B.cpp
* create intermediates
	* g++ -E B.cpp -o B.cpp.ii; g++ -E -o main.cpp.ii main.cpp 
* count lines
	* wc -l B.cpp.ii main.cpp.ii
	* 27.902